package convert

import (
	"os"
	"time"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const (
	// envDepPathMode is the name of the environment variable
	// that controls how dependency paths are rendered (EXPERIMENTAL)
	// TODO: move to CLI flags of "run" subcommand
	envDepPathMode = "DS_DEPENDENCY_PATH_MODE"
)

// Config configures how scanned files are converted
// to a Dependency Scanning report.
type Config struct {
	ScannerDetails report.ScannerDetails
	StartTime      *time.Time
	// TODO: make "run" subcommand pass dependency path mode
}

// ToReport converts dependency files returned by the Gemnasium scanner to a report.
func ToReport(scanFiles []scanner.File, cfg Config) *report.Report {
	// collect vulnerabilities and dependency files
	vulns := []report.Vulnerability{}
	depfiles := []report.DependencyFile{}
	for _, scanFile := range scanFiles {
		// add affections as vulnerabilities
		c := NewFileConverter(scanFile, depPathModeForFile(scanFile))
		vulns = append(vulns, c.Vulnerabilities()...)

		// add as dependency file if handled by package manager
		if scanFile.PackageManager != "" {
			depfiles = append(depfiles, c.DependencyFile())
		}
	}

	vulnerabilityReport := report.NewReport()
	vulnerabilityReport.Vulnerabilities = vulns
	vulnerabilityReport.DependencyFiles = depfiles
	vulnerabilityReport.Scan.Scanner = cfg.ScannerDetails
	vulnerabilityReport.Scan.Type = report.CategoryDependencyScanning
	vulnerabilityReport.Scan.Status = report.StatusSuccess
	if cfg.StartTime != nil {
		startTime := report.ScanTime(*cfg.StartTime)
		vulnerabilityReport.Scan.StartTime = &startTime
		endTime := report.ScanTime(time.Now())
		vulnerabilityReport.Scan.EndTime = &endTime
	}

	return &vulnerabilityReport
}

func depPathModeForFile(f scanner.File) DepPathMode {
	if len(f.Dependencies) == 0 {
		// dependency graph information not available
		return DepPathModeNone
	}

	// TODO: move to CLI flags of "run" subcommand
	switch os.Getenv(envDepPathMode) {
	case "all":
		return DepPathModeAll
	case "none":
		return DepPathModeNone
	default:
		return DepPathModeAffected
	}
}
