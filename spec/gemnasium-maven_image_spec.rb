require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/sbom_manifest_shared_examples'

describe "running image" do
  let(:fixtures_dir) { File.expand_path("../qa/fixtures", __dir__) }

  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-maven:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context "with test project" do
    def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
      base_dir = File.expand_path("../qa/expect", __dir__)
      path = File.join(base_dir, expectation_name, report_filename)
      JSON.parse(File.read path)
    end

    let(:global_vars) do
      {
        "GEMNASIUM_DB_REF_NAME": "v1.2.142",
        "SECURE_LOG_LEVEL": "debug"
      }
    end

    let(:project) { "any" }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }

    let(:scan) do
      target_dir = File.join(fixtures_dir, project)
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, target_dir,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    let(:report) { scan.report }

    shared_examples_for 'a complete scan' do
      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        # TODO: remove this when https://gitlab.com/gitlab-org/gitlab/-/issues/354079 is completed
        it "matches scanned files" do
          skip missing_report_message if report.nil?

          want = defined?(vulnerable_files) ? vulnerable_files.sort : scanned_files.sort
          expect(report["vulnerabilities"].map { |v| v.dig("location", "file") }.uniq.sort).to eql want
          want = scanned_files.sort
          if report.dig("scan", "type") == "dependency_scanning"
            expect(report["dependency_files"].map { |df| df["path"] }.uniq.sort).to eql want
          end
        end

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(expected_report_dir) }
        end

        it_behaves_like "valid report"
      end
    end

    context "using maven" do
      let(:project) { "java-maven/default" }
      let(:scanned_files) { ['pom.xml'] }
      let(:expected_report_dir) { 'java-maven/default' }

      context 'default configuration', run_parallel: true do
        it_behaves_like 'a complete scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "'-Dmaven.compiler.source=1.8 -Dmaven.compiler.target=1.8 -DskipTests --batch-mode'", "DS_JAVA_VERSION": "8" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 11', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "'-Dmaven.compiler.source=11 -Dmaven.compiler.target=11 -DskipTests --batch-mode'", "DS_JAVA_VERSION": "11" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 13', asdf_java: true, run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "'-Dmaven.compiler.source=13 -Dmaven.compiler.target=13 -DskipTests --batch-mode'", "DS_JAVA_VERSION": "13" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 14', asdf_java: true, run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "'-Dmaven.compiler.source=14 -Dmaven.compiler.target=14 -DskipTests --batch-mode'", "DS_JAVA_VERSION": "14" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 15', asdf_java: true, run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "'-Dmaven.compiler.source=15 -Dmaven.compiler.target=15 -DskipTests --batch-mode'", "DS_JAVA_VERSION": "15" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 16', asdf_java: true, run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "'-Dmaven.compiler.source=16 -Dmaven.compiler.target=16 -DskipTests --batch-mode'", "DS_JAVA_VERSION": "16" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 17', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "'-Dmaven.compiler.source=17 -Dmaven.compiler.target=17 -DskipTests --batch-mode'", "DS_JAVA_VERSION": "17" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'multimodule' do
        let(:project) { "java-maven/multimodules/default" }
        let(:expected_report_dir) { "java-maven/multimodules/default" }

        context 'default configuration', run_parallel: true do
          let(:scanned_files) { ["api/pom.xml", "model/pom.xml", "pom.xml", "web/pom.xml"] }

          it_behaves_like 'a complete scan'
        end

        context 'project excluded using' do
          let(:expected_report_dir) { "java-maven/multimodules/no-web" }
          let(:scanned_files) { ["api/pom.xml", "model/pom.xml", "pom.xml"] }

          context 'module dir name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web" }
            end

            it_behaves_like 'a complete scan'
          end

          context 'exported file name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web/gemnasium-maven-plugin.json" }
            end

            it_behaves_like 'a complete scan'
          end

          context 'build file name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web/pom.xml" }
            end

            it_behaves_like 'a complete scan'
          end
        end
      end
    end

    context 'using gradle' do
      let(:project) { "java-gradle/default" }
      let(:expected_report_dir) { "java-gradle/default" }
      let(:scanned_files) { ['build.gradle'] }

      context 'default configuration', run_parallel: true do
        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected default version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-17|java-17-openjdk)/)
        end
      end

      context 'path to pom excluded', run_parallel: true do
        let(:variables) { {  "DS_EXCLUDED_PATHS": "pom.xml", "DS_JAVA_VERSION": "11" } }
        let(:project) { "java-gradle/with-pom" }

        it_behaves_like 'a complete scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 8 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-8|java-1.8.0-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 11', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-11|java-11-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 13', asdf_java: true, run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 13 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-13/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 14', asdf_java: true, run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 14 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-14/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 15', asdf_java: true, run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 15 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-15/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 16', asdf_java: true, run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 16 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'adoptopenjdk-16/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '7\.3\.3'/)
        end
      end

      context 'java 17', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 17 } }

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-17|java-17-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '7\.3\.3'/)
        end
      end

      context 'gradle 5.6', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-5-6" }

        it_behaves_like 'a complete scan'
      end

      context 'gradle 6.7', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-6-7" }

        it_behaves_like 'a complete scan'
      end

      context 'gradle 6.9', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-6-9" }

        it_behaves_like 'a complete scan'
      end

      context 'gradle 7.3', run_parallel: true do
        let(:project) { "java-gradle/gradle-7-3" }

        it_behaves_like 'a complete scan'
      end

      context 'kotlin dsl', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/kotlin-dsl" }
        let(:scanned_files) { ['build.gradle.kts'] }
        let(:expected_report_dir) { 'java-gradle/kotlin-dsl' }

        it_behaves_like 'a complete scan'
      end

      context 'multimodule' do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/multimodules/default" }
        let(:scanned_files) { ["api/build.gradle", "build.gradle", "model/build.gradle", "web/build.gradle"] }
        let(:expected_report_dir) { "java-gradle/multimodules/default" }

        context 'default configuration', run_parallel: true do
          it_behaves_like 'a complete scan'
        end

        context 'customized buildfile names', run_parallel: true do
          let(:project) { "java-gradle/multimodules/subprojects-buildfilename" }

          it_behaves_like 'a complete scan'
        end

        context 'no root dependencies', run_parallel: true do
          let(:project) { "java-gradle/multimodules/no-root-dependencies" }
          let(:expected_report_dir) { "java-gradle/multimodules/no-root-dependencies" }
          let(:scanned_files) { ["api/build.gradle", "model/build.gradle", "web/build.gradle"] }
          let(:vulnerable_files) { ["api/build.gradle", "web/build.gradle"] }

          it_behaves_like 'a complete scan'
        end
      end

      context "with build containing dependency with circular references" do
        let(:recorded_report) { parse_expected_report("java-gradle/dependency-with-circular-references/implementation-directive") }

        context "and using the implementation directive" do
          let(:project) { "java-gradle/dependency-with-circular-references/implementation-directive" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report"
            it_behaves_like "valid report"
          end
        end

        context "and using the compile directive which requires gradle <= 6 and Java <= 15" do
          let(:variables) { { "DS_JAVA_VERSION": "11" } }
          let(:project) { "java-gradle/dependency-with-circular-references/compile-directive" }

          it_behaves_like "successful scan"

          describe "created report" do
            it_behaves_like "recorded report"
            it_behaves_like "valid report"
          end
        end
      end

      context "cyclone dx" do
        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-maven-gradle.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "with build using implementation directive and nested dependencies", run_parallel: true do
        let(:project) { "java-gradle/nested-dependencies-with-implementation-directive" }
        let(:expected_report_dir) { "java-gradle/nested-dependencies-with-implementation-directive" }
        let(:variables) { { "GEMNASIUM_DB_REF_NAME": "v2.0.749" } }

        it_behaves_like 'a complete scan'
      end
    end

    context 'using scala and sbt' do
      let(:project) { "scala-sbt/default" }
      let(:expected_report_dir) { "scala-sbt/default" }
      let(:scanned_files) { ["build.sbt"] }

      context 'default configuration', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 8 } }

        it_behaves_like 'a complete scan'
      end

      context 'java 11', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.0', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.0.4", "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.1', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.1.6", "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.2', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.2.8", "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.3', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.3.12" } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.4', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.4.6" } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.5', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.5.8" } }

        it_behaves_like 'a complete scan'
      end

      context 'and sbt 1.6', run_parallel: true do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.6.1" } }

        it_behaves_like 'a complete scan'
      end

      context 'multiproject', run_parallel: true  do
        let(:variables) { { "DS_JAVA_VERSION": "11" } }
        let(:project) { "scala-sbt/multiproject/default" }
        let(:expected_report_dir) { "scala-sbt/multiproject/default" }
        let(:scanned_files) { ["build.sbt", "proj1/build.sbt", "proj2/build.sbt"] }

        it_behaves_like 'a complete scan'

        describe "SBOM Manifest" do
          let(:relative_sbom_manifest_path) { "sbom-manifest.json" }

          it_behaves_like "non-empty SBOM manifest"
          it_behaves_like "recorded SBOM manifest"
        end
      end
    end
  end
end
