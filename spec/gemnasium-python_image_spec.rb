require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/sbom_manifest_shared_examples'

describe "default image", python_version: '39' do
  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-python:3")
  end

  it "runs python 3.9 by default" do
    output = `docker run -t --rm #{image_name} python3 --version`
    expect(output).to match("Python 3.9")
  end
end

describe "python-3.10 image", python_version: '310' do
  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-python:3-python-3.10")
  end

  it "runs python 3.10 by default" do
    output = `docker run -t --rm #{image_name} python3 --version`
    expect(output).to match("Python 3.10")
  end
end

describe "running image" do
  let(:fixtures_dir) { File.expand_path("../qa/fixtures", __dir__) }

  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-python:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context "with test project" do

    def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
      base_dir = File.expand_path("../qa/expect", __dir__)
      path = File.join(base_dir, expectation_name, report_filename)
      JSON.parse(File.read(path))
    end

    let(:global_vars) do
      {
        "GEMNASIUM_DB_REF_NAME": "v1.2.142",
        "SECURE_LOG_LEVEL": "debug"
      }
    end

    let(:project) { "any" }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, target_dir,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    let(:report) { scan.report }

    context "using pip" do
      context "with requirements.txt" do
        let(:project) { "python-pip/no-pillow" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["requirements.txt"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-pip.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end

        describe "SBOM Manifest" do
          let(:relative_sbom_manifest_path) { "sbom-manifest.json" }

          it_behaves_like "non-empty SBOM manifest"
          it_behaves_like "recorded SBOM manifest"
        end
      end

      context "with custom requirements filename" do
        let(:project) { "python-pip/no-pillow-custom-req-file" }

        let(:variables) do
          { "PIP_REQUIREMENTS_FILE": "cust-reqs.foo" }
        end

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["cust-reqs.foo"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-pip.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "when scanning itself" do
        let(:project) { "python-pip/self" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
        end
      end
    end

    context "using Pipenv" do
      context "without a lock file" do
        let(:project) { "python-pipenv/default" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["Pipfile"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-pipenv.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "with a lock file" do
        let(:project) { "python-pipenv/pipfile-lock" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["Pipfile"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-pipenv.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end
    end

    context "using Setuptools" do
      context "with no requirements.txt file" do
        let(:project) { "python-setuptools/default" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["setup.py"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-setuptools.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "with excluded requirements.txt file" do
        let(:project) { "python-setuptools/requirements-file" }

        it_behaves_like "successful scan"

        let(:variables) do
          { "DS_EXCLUDED_PATHS": "/requirements.txt" }
        end

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["setup.py"]

          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report("python-setuptools/default") }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-setuptools.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "fix for typing extensions bug" do
        let(:project) { "python-setuptools/361075-typing-extensions-bug" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end

          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-setuptools.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end

      context "fix for pyproject bug" do
        let(:project) { "python-setuptools/358800-pyproject-bug" }

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report("python-setuptools/default") }
          end

          it_behaves_like "valid report"
        end
      end
    end

    context "using Poetry" do
      context "with poetry.lock" do
        let(:project) { "python-poetry/default"}

        it_behaves_like "successful scan"

        describe "created report" do
          it_behaves_like "non-empty report"
          it_behaves_like "report with scanned files", ["poetry.lock"]
          it_behaves_like "recorded report" do
            let(:recorded_report) { parse_expected_report(project) }
          end
          it_behaves_like "valid report"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["cyclonedx-pypi-poetry.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end
    end
  end
end
