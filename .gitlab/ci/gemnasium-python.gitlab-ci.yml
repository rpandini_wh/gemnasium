variables:
  # Temporary image: registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium/tmp/python
  # Local image:     registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium/python
  # Official image:  registry.gitlab.com/security-products/gemnasium-python
  IMAGE_ALIAS: "python"
  ANALYZER: "gemnasium-python"

  SEC_REGISTRY_IMAGE:  "$GEMNASIUM_PYTHON_SEC_REGISTRY_IMAGE"
  SEC_REGISTRY_USER: "$GEMNASIUM_PYTHON_SEC_REGISTRY_USER"
  SEC_REGISTRY_PASSWORD: "$GEMNASIUM_PYTHON_SEC_REGISTRY_PASSWORD"

  MAX_IMAGE_SIZE_MB: 279
  MAX_IMAGE_SIZE_MB_FIPS: 1580

include:
  local: "/.gitlab/ci/image.gitlab-ci.yml"

.python 3.10:
  variables:
    DS_PYTHON_VERSION: "3.10"
    IMAGE_TAG_SUFFIX: "-python-$DS_PYTHON_VERSION"

build tmp image:
  # override job script to pass extra build argument DS_PYTHON_VERSION
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg DS_PYTHON_VERSION --build-arg GO_VERSION -t $TMP_IMAGE$IMAGE_TAG_SUFFIX -f $DOCKERFILE .
    - docker push $TMP_IMAGE$IMAGE_TAG_SUFFIX

build tmp image 3.10:
  extends:
    - "build tmp image"
    - .python 3.10

image sbom 3.10:
  extends:
    - image sbom
    - .python 3.10

tag branch 3.10:
  extends:
    - "tag branch"
    - .python 3.10

release patch 3.10:
  extends:
    - "release patch"
    - .python 3.10

release major 3.10:
  extends:
    - "release major"
    - .python 3.10

release minor 3.10:
  extends:
    - "release minor"
    - .python 3.10

release latest 3.10:
  extends:
    - "release latest"
    - .python 3.10

image test:
  script:
    - rspec spec/gemnasium-python_image_spec.rb --tag ~python_version:310

image test fips:
  script:
    - rspec spec/gemnasium-python_image_spec.rb --tag ~python_version:310

image test 3.10:
  extends:
    - "image test"
    - .python 3.10
  script:
    - rspec spec/gemnasium-python_image_spec.rb --tag ~python_version:39

.functional:
  extends: .qa-downstream-ds
  variables:
    DS_EXCLUDED_ANALYZERS: "gemnasium,gemnasium-maven,bundler-audit,retire.js"
    DS_PYTHON_VERSION: "3.9"

# Check that CI template forces DS_REPORT_PACKAGE_MANAGER_PIP_WHEN_PYTHON to false.
python-pipenv-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 22
    EXPECTATION: "python-pipenv/default"
    EXPECTED_CYCLONEDX_ARTIFACTS: "cyclonedx-pypi-pipenv.json"
  trigger:
    project: gitlab-org/security-products/tests/python-pipenv

python-pipenv-offline-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 19
    EXPECTATION: "python-pipenv/default"
  trigger:
    project: gitlab-org/security-products/tests/python-pipenv
    branch: offline-FREEZE

python-pipenv-offline-qa fips:
  extends: python-pipenv-offline-qa
  variables:
    IMAGE_TAG_SUFFIX: "-fips"

# Test integration with a custom build job that builds Python Wheels for default version of Python.
# The build job provides the system libraries required to build Pillow, but the scanning job doesn't.
python-pip-qa:
  extends: .functional
  variables:
    MAX_SCAN_DURATION_SECONDS: 19
    EXPECTATION: "python-pip/default"
  trigger:
    project: gitlab-org/security-products/tests/python-pip

python-pip-qa fips:
  extends: python-pip-qa
  variables:
    IMAGE_TAG_SUFFIX: "-fips"

.functional 3.10:
  extends:
    - .qa-downstream-ds
    - .python 3.10

python-pipenv-offline-qa 3.10:
  extends:
    - python-pipenv-offline-qa
    - .python 3.10
