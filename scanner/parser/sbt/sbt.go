package sbt

import (
	"fmt"
	"io"
	"io/ioutil"
	"strings"

	"gonum.org/v1/gonum/graph"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// pkgnodeMap is a map keyed to the unique id of pkgnodes
type pkgnodeMap map[string]*pkgnode

// pkgnode stores references to the dotGraph node and the parser.Package
// instantiated from this node
type pkgnode struct {
	n *dotNode
	p *parser.Package
}

// add adds the graph node to the map by create a representative pkgnode
// and keying it to the graph.Node's underlying dotNode id
func (pm pkgnodeMap) add(gn graph.Node) {
	n := gn.(*dotNode)
	if n.evictedBy != nil {
		n = n.evictedBy
	}
	if _, ok := pm[n.id]; !ok {
		p := &parser.Package{
			Name:    strings.Join(strings.Split(n.id, ":")[:2], "/"),
			Version: strings.Split(n.id, ":")[2],
		}
		pm[n.id] = &pkgnode{n, p}
	}
}

// get retrieves the corresponding pkgnode given a graph.Node
// that is used from which its is extracted
func (pm pkgnodeMap) get(gn graph.Node) (*pkgnode, error) {
	n := gn.(*dotNode)
	if n.evictedBy != nil {
		n = n.evictedBy
	}
	p, ok := pm[n.id]
	if !ok {
		return nil, fmt.Errorf("unknown node passed: %+v", gn)
	}
	return p, nil
}

func readGraph(r io.Reader) (*dotGraph, error) {
	// read dot input
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	// create new dotGraph
	g, err := newDotGraph(bs)
	if err != nil {
		return nil, err
	}

	// Sbt dependency graphs are exported to DOT format with project as root node
	// but this should not be reported in the dependency or package list.
	g.RemoveNode(0)

	return g, nil
}

func parseGraph(g *dotGraph) ([]parser.Package, []parser.Dependency, error) {
	// add packages
	pm := pkgnodeMap{}
	for _, n := range graph.NodesOf(g.Nodes()) {
		pm.add(n)
	}

	// toset stores the set of all incident edges encountered while processing
	// edges
	toset := make(map[string]struct{})

	// add dependencies
	var deps []parser.Dependency
	for _, e := range graph.EdgesOf(g.Edges()) {
		ee := e.(*dotEdge)

		// skip evictionEdge edges as they are accounted for when adding a pkgnode
		if ee.evictionEdge {
			continue
		}

		p1, err := pm.get(ee.From())
		if err != nil {
			return nil, nil, err
		}
		p2, err := pm.get(ee.To())
		if err != nil {
			return nil, nil, err
		}
		dep := parser.Dependency{
			From:         p1.p,
			To:           p2.p,
			VersionRange: p2.p.Version,
		}
		deps = append(deps, dep)
		toset[p2.n.id] = struct{}{}
	}

	// if a pkgnode does not appear in the toset it means it has
	// no incident edges in the graph (i.e. it is a top-level requirement
	// in the build file) and must be added as a root dependency
	// separately
	pkgs := []parser.Package{}
	for id, p := range pm {
		pkgs = append(pkgs, *p.p)
		if _, ok := toset[id]; !ok {
			dep := parser.Dependency{
				To:           p.p,
				VersionRange: p.p.Version,
			}
			deps = append(deps, dep)
			toset[id] = struct{}{}
		}
	}

	return pkgs, deps, nil
}

// Parse scans the dot report generated by sbt-dependency-graph plugin
// and returns a list of packages and dependencies
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	g, err := readGraph(r)
	if err != nil {
		return nil, nil, err
	}
	return parseGraph(g)
}

func init() {
	parser.Register("sbt", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeMaven,
		Filenames:   []string{"dependencies-compile.dot"},
	})
}
