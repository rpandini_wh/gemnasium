package piplock

import (
	"encoding/json"
	"io"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// DepInfo contains version and index
type DepInfo struct {
	Version string `json:"version"`
	Index   string `json:"index,omitempty"`
}

// Dependencies is a map of DepInfos
type Dependencies map[string]DepInfo

// Document is loaded with pipefile information
type Document struct {
	Meta struct {
		PipfileSpec   int    `json:"pipfile-spec,omitempty"`
		PipfileSha256 string `json:"Pipfile-sha256,omitempty"` // used before pipfile-spec was introduced
	} `json:"_meta"`
	Default Dependencies `json:"default"`
	Develop Dependencies `json:"develop"`
}

// version returns the version number, without the leading "==".
func (n DepInfo) version() string {
	return strings.TrimPrefix(n.Version, "==")
}

// Parse scans a Pipenv lock file and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	document := Document{}
	if err := json.NewDecoder(r).Decode(&document); err != nil {
		return nil, nil, err
	}
	// In pipenv 6.x and later, the _meta section of the Pipefile.lock
	// contains a pipfile-spec (integer) and a hash.sha256 (string).
	// Older versions of Pipfile.lock don't have these fields
	// but have a _meta.Pipfile-sha256 instead.
	// All these flavors of Pipfile.lock are supported.
	// See https://github.com/pypa/pipenv/blob/v6.0.3/Pipfile.lock
	// and https://github.com/pypa/pipenv/blob/v0.3.0/Pipfile.lock
	if document.Meta.PipfileSpec == 0 && document.Meta.PipfileSha256 == "" {
		return nil, nil, parser.ErrWrongFileFormat
	}
	var result []parser.Package
	for depName, depInfo := range document.Default {
		delete(document.Develop, depName) // Remove possible duplicate from the dev dependencies
		result = append(result, parser.Package{Name: depName, Version: depInfo.version()})
	}

	for depName, depInfo := range document.Develop {
		result = append(result, parser.Package{Name: depName, Version: depInfo.version()})
	}
	return result, nil, nil
}

func init() {
	parser.Register("piplock", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypePypi,
		Filenames:   []string{"Pipfile.lock"},
	})
}
